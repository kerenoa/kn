#include "Room.h"

/*
Input: room id, room name, the admin of the room, the maximum users of the room, the number of questions and the time for each question
The function is a c'tor
*/
Room::Room(int id, User* admin, string name, int maxUsers, int questionNo, int questionTime)
{
	_id = id;
	_admin = admin;
	_name = name;
	_maxUsers = maxUsers;
	_questionNo = questionNo;
	_questionTime = questionTime;
	_users.push_back(admin);
}

Room::~Room()
{
	_users.clear();
}

/*
Input: none
Output: vector of user objects
The function returns the users vector
*/
vector<User*> Room::getUsers()
{
	return _users;
}

/*
Input: none
Output: the number of questions
The function returns the number of questions in this room
*/
int Room::getQuestionsNo()
{
	return _questionNo;
}

/*
Input: none
Output: id
The function returns the room id
*/
int Room::getId()
{
	return _id;
}

/*
Input: none
Output: name
The function returns the room name
*/
string Room::getName()
{
	return _name;
}

string Room::getUsersListMessage()
{
	string msg = to_string(_users.size());
	for (size_t i = 0; i < _users.size(); i++)
	{
		User* user = _users.at(i);
		string username = user->getUsername();
		string size = to_string((username).size());

		while (size.length() != 2)
		{
			size = "0" + size;
		}

		msg += size;
		msg += user->getUsername();
	}
	return msg;
}

void Room::leaveRoom(User* excludeUser)
{
	size_t i = 0;
	bool isFound = false;

	for (i = 0; i < _users.size() && isFound == false; i++)
	{
		if (_users[i] == excludeUser)
		{
			isFound = true;
		}
	}

	if (isFound)
	{
		_users.erase(_users.begin() + i - 1);
		excludeUser->send("1120");

		string usersList = "108" + getUsersListMessage();
		sendMessage(usersList);
	}
}

bool Room::joinRoom(User* excludeUser)
{
	bool succeeded = false;

	if (_users.size() == _maxUsers)
	{
		excludeUser->send("1101");
	}
	else
	{
		string questionsNo = to_string(_questionNo), questionTime = to_string(_questionTime);

		while (questionsNo.length() != 2)
		{
			questionsNo = "0" + questionsNo;
		}

		while (questionTime.length() != 2)
		{
			questionTime = "0" + questionTime;
		}

		string successMessage = "1100";
		successMessage += questionsNo;
		successMessage += questionTime;
		_users.push_back(excludeUser);
		excludeUser->send(successMessage);

		string usersList = "108" + getUsersListMessage();
		sendMessage(usersList);

		succeeded = true;
	}

	return succeeded;
}

void Room::sendMessage(string message)
{
	sendMessage(nullptr, message);
}

void Room::sendMessage(User* excludeUser, string message)
{
	try {
		for (size_t i = 0; i < _users.size(); i++)
		{
			if (_users[i] != excludeUser)
			{
				_users[i]->send(message);
			}
		}
	}
	catch (...) {}
}

int Room::closeRoom(User* user)
{
	string closeMsg = "116";
	int answer = -1;

	if (_admin->getUsername().compare(user->getUsername()) == 0)
	{
		sendMessage(closeMsg);
		answer = _id;
		for (size_t i = 0; i < _users.size(); i++)
		{
			if (_admin != user)
			{
				(_users.at(i))->clearRoom();
			}
		}
	}

	return answer;
}