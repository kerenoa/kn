#pragma once

#include <vector>
#include <map>
#include <string>
#include "Question.h"
#include "DataBase.h"
#include "User.h"

using namespace std;

class User;

class Game
{
private:
	vector<Question*> _questions;
	vector<User*> _players;
	int _questionsNo;
	int _currQuestionIndex;
	DataBase& _db;
	map<string, int> _results;
	int _currentTurnAnswers;

	/*bool insertGameToDB();
	void initQuestionsFromDB();
	*/void sendQuestionToAllUsers();
	

public:
	Game(const vector<User*>& players, int questionsNo, DataBase& db);
	~Game();
	void sendFirstQuestion();
	/*void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	bool leaveGame(User* currUser); */
	int getId();
};
