#include "Validator.h"

bool Validator::isPasswordValid(string password)
{
	bool len = password.length() >= 4;
	cout << len << endl;
	bool noSpaces = password.find(" ") == string::npos;
	cout << noSpaces << endl;
	bool oneDigit = (password.find_first_of("0123456789") != std::string::npos);
	cout << oneDigit << endl;
	bool oneUpperCase = (password.find_first_of("ABCDEFGHIJKLMNOPQRSTUVWXYZ") != std::string::npos);
	cout << oneUpperCase << endl;
	bool oneLowerCase = (password.find_first_of("abcdefghijklmnopqrstuvwxyz") != std::string::npos);
	cout << oneLowerCase << endl;

	return len && noSpaces && oneDigit && oneUpperCase && oneLowerCase;
}

bool Validator::isUserNameValid(string username)
{
	bool notEmpty = username.length() > 0;
	bool noSpaces = username.find(" ") == string::npos;
	bool firstIsLetter = (((username.substr(0, 1)).find_first_of("ABCDEFGHIJKLMNOPQRSTUVWXYZ") != std::string::npos) || ((username.substr(0, 1)).find_first_of("abcdefghijklmnopqrstuvwxyz") != std::string::npos));

	return notEmpty && noSpaces && firstIsLetter;
}
