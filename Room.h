#pragma once

#include <vector>
#include <iostream>
#include <string>
#include "User.h"
class User;
using namespace std;

class Room
{
private:
	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	string _name;
	int _id;

	//string getUsersAsString(vector<User*> usersList, User* excludeUser);
	void sendMessage(string msg);
	void sendMessage(User* excludeUser, string msg);


public:
	Room(int id, User* admin, string name, int maxUsers, int questionNo, int questionTime);
	~Room();
	bool joinRoom(User* excludeUser);
	void leaveRoom(User* excludeUser);
	int closeRoom(User* excludeUser);
	vector<User*> getUsers();
	string getUsersListMessage();
	int getQuestionsNo();
	int getId();
	string getName();

};
