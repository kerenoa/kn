#pragma once

#include "RecievedMessage.h"
#include "DataBase.h"
#include "Room.h"
#include "Validator.h"
#include "Game.h"
#include <iostream>
#include <map>
#include <mutex>
#include <thread>
#include <queue>


#define PORT 8820

class TriviaServer
{
private:
	SOCKET _socket;
	std::map<SOCKET, User*> _connectedUsers;
	DataBase* _db;
	map<int, Room*> _roomList;
	std::mutex _mtxRecievedMessages;
	std::queue<RecievedMessage*> _queRcvMessages;
	static int _roomIdSequence;

	void bindAndListen();
	void accept();
	void clientHandler(SOCKET clientSocket);
	void safeDeleteUser(RecievedMessage* msg);
	User* handleSignin(RecievedMessage* msg); //keren
	bool handleSignup(RecievedMessage* msg); //keren
	void handleSignout(RecievedMessage* msg);
	//void handleLeaveGame(RecievedMessage* msg);
	void handleStartGame(RecievedMessage* msg);
	//void handlePlayerAnswer(RecievedMessage* msg);*/
	bool handleCreateRoom(RecievedMessage* msg);
	bool handleCloseRoom(RecievedMessage* msg); //
	bool handleJoinRoom(RecievedMessage* msg); //
	bool handleLeaveRoom(RecievedMessage* msg); //
	void handleGetUsersInRoom(RecievedMessage* msg); //
	void handleGetRooms(RecievedMessage* msg); //
											   //void handleGetBestScores(RecievedMessage* msg);
											   //void handleGetPersonalStatus(RecievedMessage* msg);*/
	void handleRecievedMessages();
	void addRecievedMessage(RecievedMessage* msg);
	RecievedMessage* buildRecievedMessage(SOCKET socket, int msgTypeCode, std::string msg);
	User* getUserByName(string username);
	User* getUserBySocket(SOCKET clientSocket);

	Room* getRoomById(int roomId);

public:
	TriviaServer();
	~TriviaServer();
	void server();
};
