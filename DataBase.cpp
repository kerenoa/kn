#include "DataBase.h"

DataBase::DataBase()
{

}

DataBase::~DataBase()
{
	_db.clear();
}

/*
Input: username
Output: boolean
The function checks if the user exists in the database
*/
bool DataBase::isUserExists(string username)
{
	bool ans = true;

	for (size_t i = 0; i < _db.size(); i++)
	{
		if (_db[i][0].compare(username) == 0)
		{
			ans = false;
		}
	}

	return ans;
}


bool DataBase::addNewUser(string username, string password, string email)
{
	vector<string> user;
	user.push_back(username);
	user.push_back(password);
	user.push_back(email);
	_db.push_back(user);
	return true; //For now - using temporary vector can't make problems while adding another user to the database
}


bool DataBase::isUserAndPassMatch(string username, string password)
{
	bool answer = false;
	for (size_t i = 0; i < _db.size() && answer == false; i++)
	{
		answer = (_db[i][0].compare(username) == 0 && _db[i][1].compare(password) == 0) ? true : false;
	}
	return answer;
}
/*
vector<Question*> DataBase::initQuestions(int questionNo)
{
}

vector<string> DataBase::getBestScores()
{

}

vector<string> DataBase::getPersonalStatus(string username)
{

}

int DataBase::insertNewGame()
{

}

bool DataBase::updateGameStatus(int gameId)
{

}

bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{

}
*/