#include <vector>
#include "RecievedMessage.h"

using namespace std;

/*
	Input: client socket, message code and the values from the message the client sent
	The function is a c'tor that initiate the fields
*/
RecievedMessage::RecievedMessage(SOCKET clientSocket, int msgCode, vector<string> values)
{
	_sock = clientSocket;
	_messageCode = msgCode;
	_values = values;
	_user = nullptr;
}

/*
	Input: none
	The function is a d'tor
*/
RecievedMessage::~RecievedMessage()
{
	_values.clear();
}

/*
	Input: none
	Output: socket
	The function returns the message socket
*/
SOCKET RecievedMessage::getSock()
{
	return _sock;
}

/*
	Input: none
	Ouptut: pointer to a user object
	The function returns the user who sent the message
*/
User* RecievedMessage::getUser()
{
	return _user;
}

/*
	Input: none
	Output: vector of strings
	The function returns all the values from the message
*/
std::vector<std::string>& RecievedMessage::getValues()
{
	return _values;
}

/*
	Input: client socket and message code
	The function is a c'tor that initiate the fields
*/
RecievedMessage::RecievedMessage(SOCKET clientSocket, int msgCode)
{
	_sock = clientSocket;
	_messageCode = msgCode;
	_user = nullptr;
}

/*
	Input: user object
	Output: none
	The function initiate the user by the parameter
*/
void RecievedMessage::setUser(User* user)
{
	_user = user;
}

/*
	Input: none
	Output: message code
	The function return the message code of this message
*/
int RecievedMessage::getMessageCode()
{
	return _messageCode;
}