#pragma once

#include "Helper.h"
#include "Room.h"
#include "Game.h"
class Game;
class Room;
using namespace std;

class User
{
private:
	std::string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;

public:
	User(string username, SOCKET sock);
	~User();
	void send(string msg);
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game* gm);
	void clearRoom();
	bool joinRoom(Room* newRoom);
	void leaveRoom();
	int closeRoom();
	void clearGame();
	//bool leaveGame();
	bool createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime);
};
