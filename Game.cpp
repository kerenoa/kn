#include "Game.h"

Game::Game(const vector<User*>& players, int questionsNo, DataBase& db) : _db(db)
{
	int i = _db.insertNewGame();
	if (i == -1)
	{
		throw exception();
	}

	_currQuestionIndex = 0;
	_db.initQuestions(questionsNo);
	_players = players;
	_questionsNo = questionsNo;
	for (size_t i = 0; i < _players.size(); i++)
	{
		_players[i]->setGame(this);
	}
}

/*
	Input: none
	The function is a d'tor
*/
Game::~Game()
{
	_questions.clear();
	_players.clear();
}

/*//What Id?
int Game::getId()
{
	return 2;
}*/

void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}

void Game::sendQuestionToAllUsers()
{
	string msg = "118";
	_currentTurnAnswers = 0;
	
	Question* question = _questions[_currQuestionIndex];
	string q = question->getQuestion();
	string answer1 = question->getAnswers()[0];
	string answer2 = question->getAnswers()[1];
	string answer3 = question->getAnswers()[2];
	string answer4 = question->getAnswers()[3];

	string qLen = padding(to_string(q.length()),3);
	string a1Len = padding(to_string(answer1.length()),3);
	string a2Len = padding(to_string(answer2.length()),3);
	string a3Len = padding(to_string(answer3.length()),3);
	string a4Len = padding(to_string(answer4.length()),3);

	msg += qLen + q + a1Len + answer1 + a2Len + answer2 + a3Len + answer3 + a4Len + answer4;

	for (size_t i = 0; i < _players.size(); i++)
	{
		try 
		{
			_players[i]->send(msg);
		}
		catch (...) {}
	}
}

string padding(string str, int num)
{
	string newStr = str;
	while (newStr.length() != num)
	{
		newStr = "0" + newStr;
	}
	return newStr;
}