#pragma once

#include <vector>
#include "User.h"

class RecievedMessage
{
private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	std::vector<std::string> _values;

public:
	RecievedMessage(SOCKET clientSocket, int msgCode);
	RecievedMessage(SOCKET clientSocket, int msgCode, std::vector<std::string> values);
	~RecievedMessage();
	SOCKET getSock();
	User* getUser();
	void setUser(User* user);
	int getMessageCode();
	std::vector<std::string>& getValues();
};
