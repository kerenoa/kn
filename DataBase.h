#pragma once

#include <vector>
#include <string>
#include "Question.h"
using namespace std;

class DataBase
{
private:
	vector<vector<string>> _db;
	vector<Question*> _questions;
	/*static int callbackCount(void* v, int i, char** c1, char** c2);
	static int callbackQuestions(void* v, int i, char** c1, char** c2);
	static int callbackBestScores(void* v, int i, char** c1, char** c2);
	static int callbackPersonalStatus(void* v, int i, char** c1, char** c2);
	*/

public:
	DataBase();
	~DataBase();
	bool isUserExists(string username);
	bool addNewUser(string username, string password, string email);
	bool isUserAndPassMatch(string username, string password); //keren
	vector<Question*> initQuestions(int questionNo);
	//vector<string> getBestScores();
	//vector<string> getPersonalStatus(string s);
	int insertNewGame();
	//bool updateGameStatus(int i);
	//bool addAnswerToPlayer(int i, string s, int i1, string s1, bool b, int i2);
};
